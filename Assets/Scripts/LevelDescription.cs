﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

public class LevelDescription 
{
    [XmlAttribute]
    public string Name
    {
        get;
        set;
    }

    [XmlAttribute]
    public EnemyType Enemies
    {
        get;
        set;
    }

    [XmlAttribute]
    public string LevelName
    {
        get;
        set;
    }
}
