﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuManager : MonoBehaviour
{

    [SerializeField]
    GameObject MenuPanel;

    [SerializeField]
    GameObject LevelPanel;

    // Start is called before the first frame update
    void Start()
    {
        LevelPanel.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    
    public void StartGame(string level)
    {
       
        SceneManager.LoadScene(level);
    }

    public void SelectLevel()
    {
         MenuPanel.SetActive(false);
        LevelPanel.SetActive(true);
    }
}
