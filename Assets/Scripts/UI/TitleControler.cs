﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleControler : MonoBehaviour
{
    Text instruction;
    void Start () {
        instruction = GetComponent<Text>();
        InvokeRepeating("EnlargeText", 0.1f, 0.091f);

    }

    // Update is called once per frame
    void Update()
    {
    }

    void EnlargeText(){
        instruction.fontSize += 1;
        if(instruction.fontSize >= 157)
        {
            CancelInvoke("EnlargeText");
        }
    }
}
