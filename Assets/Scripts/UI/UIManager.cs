﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    Slider energySlider;

    [SerializeField]
    GameObject GameManager;

    [SerializeField]
    Slider EnergySlider;

    [SerializeField]
    Slider HealthSlider;

    [SerializeField]
    Text DeathText;

    // Start is called before the first frame update
    void Start()
    {
        PlayerAvatar.onDeathEvent += ManageDeath;
        InvokeRepeating("PollPlayer", 0.1f, 0.15f);
    }

    void OnDestroy()
    {
        PlayerAvatar.onDeathEvent -= ManageDeath;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PollPlayer()
    {
        //energySlider = GetComponent<Slider>();
        GameObject player = GameManager.GetComponent<GameManagerScript>().getPlayerInstance();
        if(player == null)
        {
            CancelInvoke("PollPlayer");
        }
        else{
            HealthSlider.value = player.GetComponent<PlayerAvatar>().GetHealth();
            EnergySlider.value = player.GetComponent<PlayerAvatar>().GetEnergy();
        }
    }

    public void ManageDeath()
    {
        DeathText.GetComponent<Text>().text = "GameOver";
    }
}
