﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction {Up, Down, Left, Right, Void};

public class InputController : MonoBehaviour
{
    Direction lastInput = Direction.Void;
    float lastTime;
    float upTime = 0.0f;
    float rightTime = 0.0f;
    float leftTime = 0.0f;
    float downTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        

        //ImplémenterlecomposantInputControllerquicontrôlelaSpeeddesEngines(qu’ilfaut
        //donc recupérer...) à l’aide des Input Axis
        float verticalInput = Input.GetAxis("Vertical");
        float horizontalInput = Input.GetAxis("Horizontal");
        gameObject.GetComponent<Engines>().setSpeed(new Vector2(horizontalInput,verticalInput));

        if (Input.GetKeyUp("space"))
        {
            GetComponent<BulletGun>().Fire();
        }

        if(Input.GetKeyDown(KeyCode.Tab))
        {
            GetComponent<BulletGun>().SwitchBullet();
        }


        if(Input.GetKeyDown(KeyCode.W))
        {
            if(upTime > 0.0f)
            {
                upTime = Time.time - upTime;
                if(upTime <= 0.5f)
                {
                    gameObject.GetComponent<Engines>().Roll(new Vector3(0,1,0));
                }
                upTime = 0.0f;
            }
            else
            {
                upTime = Time.time;   
            }
        }
        if(Input.GetKeyUp(KeyCode.A))
        { 
            rightTime = Time.time;
        }
        if(Input.GetKeyDown(KeyCode.A) && rightTime > 0.0f)
        {
            rightTime = Time.time - rightTime;
            if(rightTime <= 0.7f)
            {
                gameObject.GetComponent<Engines>().Roll(new Vector3(-1,0,0));
            }
            rightTime = 0.0f;
        }
        if(Input.GetKeyUp(KeyCode.D))
        { 
            leftTime = Time.time;
        }
        if(Input.GetKeyDown(KeyCode.D) && leftTime > 0.0f)
        {
            leftTime = Time.time - leftTime;
            if(leftTime <= 0.7f)
            {
                gameObject.GetComponent<Engines>().Roll(new Vector3(1,0,0));
            }
            leftTime = 0.0f;
        }
        if(Input.GetKeyUp(KeyCode.S))
        { 
            downTime = Time.time;
        }
        if(Input.GetKeyDown(KeyCode.S) && downTime > 0.0f)
        {
            downTime = Time.time - downTime;
            if(downTime <= 0.7f)
            {
                gameObject.GetComponent<Engines>().Roll(new Vector3(0,-1,0));
            }
            downTime = 0.0f;
        }
    }

    
}

        /*if(lastInput == Direction.Void)
        {
            lastTime = Time.timeSinceLevelLoad;
            if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
            {
                lastInput = Direction.Left;
            }
            if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow))
            {
                lastInput = Direction.Up;
            }
            if(Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            {
                lastInput = Direction.Right;
            }
            if(Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                lastInput = Direction.Down;
            }
        }
        else
        {
            if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if(lastInput == Direction.Left && (Time.timeSinceLevelLoad-lastTime ) < 0.1)
                {
                    gameObject.GetComponent<Engines>().Roll(new Vector3(-5,0,0));
                }
            }
            if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                if(lastInput == Direction.Left && (Time.timeSinceLevelLoad-lastTime ) < 0.1)
                {
                    gameObject.GetComponent<Engines>().Roll(new Vector3(-5,0,0));
                }
            }
            if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                if(lastInput == Direction.Left && (Time.timeSinceLevelLoad-lastTime ) < 0.1)
                {
                    gameObject.GetComponent<Engines>().Roll(new Vector3(-5,0,0));
                }
            }
            if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                if(lastInput == Direction.Left && (Time.timeSinceLevelLoad-lastTime ) < 0.1)
                {
                    gameObject.GetComponent<Engines>().Roll(new Vector3(-5,0,0));
                }
            }
        }*/