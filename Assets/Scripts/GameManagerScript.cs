﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    Level CurrentLevel;

    LevelDescription Data;

    [SerializeField]
    GameObject playerPrefab;

    [SerializeField]
    GameObject enemyPrefab;

    [SerializeField]
    float enemyFrequency = 5.0f;

    GameObject player;

    [SerializeField]
    TextAsset levelDatabase;

    List<LevelDescription> levelDescriptions;

    private int currentLevelIndex;

    private IEnumerator StartGame()
    {
        levelDescriptions = XmlHelpers.DeserializeDatabaseFromXML<LevelDescription>(levelDatabase);
    
        yield break;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = Instantiate(playerPrefab,new Vector3(-4,0,0),Quaternion.identity);
        PlayerAvatar.onDeathEvent += ManageDeath;
        
        InvokeRepeating("DeployGarrison", 1.0f, enemyFrequency);
    }
    
    void OnDestroy()
    {
        PlayerAvatar.onDeathEvent -= ManageDeath;
    }


    // Update is called once per frame
    void Update()
    {
    }

    void DeployGarrison()
    {
        (EnemyFactory.Instance.GetEnemy(EnemyType.ZigZagEnemy)).transform.position = new Vector3(8,Random.Range(-4.0f,4.0f),0);
    }

    public GameObject getPlayerInstance()
    {
        return player;
    }

    public void ManageDeath()
    {
        Debug.Log("Someone Died?");
    }

    public void ExecuteCurrentLevel()
    {

    }

    private void StartNextLevel()
    {
        // get next level in database...
        this.currentLevelIndex++;;

        if(this.currentLevelIndex >= this.levelDescriptions.Count)
        {
            return;
        }
        var levelDescription = this.levelDescriptions[currentLevelIndex];
    }
}
