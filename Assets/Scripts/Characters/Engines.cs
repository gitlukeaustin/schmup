﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engines : MonoBehaviour
{
    [SerializeField]
    public float MoveSpeed = 8.0f;

    [SerializeField]
    public Vector2 AxisSpeed;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //calcul à chaque frame de la nouvelle position en fonction de la MaxSpeed 
        //de l’avatar(et du Frame Delta)
        this.transform.position += new Vector3(MoveSpeed * AxisSpeed.x * Time.deltaTime,MoveSpeed * AxisSpeed.y * Time.deltaTime,0);
    }

    public void setSpeed(Vector2 s)
    {
        // speed is axis value!?
        this.AxisSpeed = s;
    }

    public void Roll(Vector3 direction)
    {
        Debug.Log("Rolling");
        GetComponent<PlayerAvatar>().DecreaseEnergy(0.4f);
        this.transform.position += direction;
        StartCoroutine(BeInvicible());
        GetComponent<BoxCollider2D>().enabled = false;
    }

    public IEnumerator BeInvicible()
    {
        float counter = 1.0f;
        while(counter > 0)
        {
            GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
            yield return new WaitForSeconds(0.05f);
            counter -= 7*Time.deltaTime;
        }
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = true;

        yield return 1;
    }
}
