﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class _UnityEventFloat:UnityEvent<float> {}


public class PlayerAvatar : BaseComponent
{
    public delegate void OnDeathEvent();
    public static event OnDeathEvent onDeathEvent;

    float Energy = 1.0f;

    [SerializeField]
    float RechargeRate = 0.2f;

    bool locked = false;

    // Start is called before the first frame update
    void Start()
    {
        Health = MaxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float GetHealth()
    {
        return Health;
    }

    public float GetEnergy()
    {
        return Energy;
        
    }

    public void DecreaseEnergy(float amount)
    {
        Energy -= amount;
        CancelInvoke("RestoreEnergy");
        if(Energy <= 0)
        {
            InvokeRepeating("RestoreEnergy", 0.001f, RechargeRate);
        }
        else
        {
            locked = true;
            InvokeRepeating("RestoreEnergy", 0.001f, RechargeRate * 0.25f);
        }
    }

    void RestoreEnergy()
    {
        if(Energy >= 1.0f)
        {
            CancelInvoke("RestoreEnergy");
            locked = false;
        }
        else{
            Energy += 0.02f;
        }
    }




    public override void ManageZeroHealth()
    {
        onDeathEvent();
    }
}
