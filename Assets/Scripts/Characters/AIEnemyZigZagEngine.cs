﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyZigZagEngine : MonoBehaviour
{
    float ZigZag = 1;

    [SerializeField]
    float MoveSpeed = 5;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ToggleZigZag",0.0f,0.4f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += new Vector3(MoveSpeed * -1 * Time.deltaTime,MoveSpeed * ZigZag * Time.deltaTime,0);
    }

    void ToggleZigZag()
    {
        ZigZag *= -1;
    }
}
