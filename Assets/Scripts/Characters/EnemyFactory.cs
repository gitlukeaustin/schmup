﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : MonoBehaviour
{
    [SerializeField]
    GameObject enemyPrefab;

    [SerializeField]
    GameObject zigZagEnemyPrefab;

    private readonly Queue<EnemyAvatar> enemyCache = new Queue<EnemyAvatar>();
    private readonly Queue<EnemyAvatar> zigZagEnemyCache = new Queue<EnemyAvatar>();

   
    public GameObject GetEnemy(EnemyType type)
    {
        GameObject b;
        
        if(type == EnemyType.DefaultEnemy)
        {
            if(enemyCache.Count == 0)
            {
                b = (GameObject)GameObject.Instantiate(this.enemyPrefab);
            }
            else
            {
                b = enemyCache.Dequeue().gameObject;
            }
            b.SetActive(true);
        }
        else // if type == EnemyType.ZigZagEnemy
        {
            if(zigZagEnemyCache.Count == 0)
            {
                b = (GameObject)GameObject.Instantiate(this.zigZagEnemyPrefab);
            }
            else
            {
                b = zigZagEnemyCache.Dequeue().gameObject;
            }
            b.SetActive(true);
        }
        return b;
    }

    public void Release(EnemyAvatar enemy)
    {
        
        enemy.gameObject.SetActive(false);
        
        if(enemy.GetEnemyType() == EnemyType.DefaultEnemy)
        {
            enemyCache.Enqueue(enemy);
        }
        else if(enemy.GetEnemyType() == EnemyType.ZigZagEnemy)
        {
            zigZagEnemyCache.Enqueue(enemy);
        }

    }

    public static EnemyFactory Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        EnemyFactory.Instance = this;
    }
}
