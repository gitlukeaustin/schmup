﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType {DefaultEnemy,Friend, ZigZagEnemy};

public class EnemyAvatar : BaseComponent
{
    // Start is called before the first frame update
    void Start()
    {
        Health = MaxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ManageZeroHealth()
    {
        EnemyFactory.Instance.Release(this);
    }

    void OnBecameInvisible()
    {
        //Destroy(gameObject);
        EnemyFactory.Instance.Release(this);
    }

    public EnemyType GetEnemyType()
    {
        return EnemyType.DefaultEnemy;
    }

}
