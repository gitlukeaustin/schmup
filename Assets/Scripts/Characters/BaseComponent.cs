﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseComponent : MonoBehaviour
{
 

    [SerializeField]
    protected float MaxSpeed = 5;

    [SerializeField]
    protected float Health = 1.0f; 
    
    [SerializeField]
    protected float MaxHealth;
    
    public void TakeDamage(float damage)
    {
        Health -= damage;

        if(Health <= 0)
        {
            ManageZeroHealth();
        }
    }
    
    public virtual void ManageZeroHealth()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {  
    }

    
    public float getSpeed()
    {
        return MaxSpeed;
    }
}
