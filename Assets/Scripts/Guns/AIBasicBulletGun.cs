﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBasicBulletGun : MonoBehaviour
{

    [SerializeField]
    float FiringRate = 1.0f;

    [SerializeField]
    GameObject gameManager;


    // Start is called before the first frame update
    void Start()
    {
    }

    void OnEnable()
    {
        StartCoroutine("Fire");
    }

    // Update is called once per frame
    void Update()
    {
    }

    public IEnumerator Fire()
    {
        while(true)
        {
            (BulletFactory.Instance.GetBullet(BulletType.EnemyBullet)).transform.position = transform.position;
            yield return new WaitForSeconds(FiringRate);
        }
    }


}
