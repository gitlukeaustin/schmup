﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletFactory : MonoBehaviour
{
    [SerializeField]
    GameObject enemyBulletPrefab;

    [SerializeField]
    GameObject playerBulletPrefab;

    [SerializeField]
    GameObject diagonalBulletPrefab;


    [SerializeField]
    GameObject spiralBulletPrefab;

    private readonly Queue<Bullet> playerBulletCache = new Queue<Bullet>();
    private readonly Queue<Bullet> diagonalBulletCache = new Queue<Bullet>();
    private readonly Queue<Bullet> spiralBulletCache = new Queue<Bullet>();

    private readonly Queue<Bullet> enemyBulletCache = new Queue<Bullet>();

   
    public GameObject GetBullet(BulletType type)
    {
        GameObject b;
        if(type == BulletType.EnemyBullet)
        {
            if(enemyBulletCache.Count == 0)
            {
                b = (GameObject)GameObject.Instantiate(this.enemyBulletPrefab);
            }
            else
            {
                b = enemyBulletCache.Dequeue().gameObject;
                b.SetActive(true);
            }
            //b = Instantiate(enemyBulletPrefab,transform.position,Quaternion.identity);
        }
        else if(type == BulletType.DiagonalBullet)
        {
            if(diagonalBulletCache.Count == 0)
            {
                b = (GameObject)GameObject.Instantiate(this.diagonalBulletPrefab);
            }
            else
            {
                b = diagonalBulletCache.Dequeue().gameObject;
                b.SetActive(true);
            }
        }
        else if(type == BulletType.SpiralBullet)
        {
            if(spiralBulletCache.Count == 0)
            {
                b = (GameObject)GameObject.Instantiate(this.spiralBulletPrefab);
            }
            else
            {
                b = spiralBulletCache.Dequeue().gameObject;
                b.SetActive(true);
            }
        }
        else 
        {
            if(playerBulletCache.Count == 0)
            {
                b = (GameObject)GameObject.Instantiate(this.playerBulletPrefab);
            }
            else
            {
                b = playerBulletCache.Dequeue().gameObject;
                b.SetActive(true);
            }
            //b = Instantiate(playerBulletPrefab,gameObject.GetComponent<GameManagerScript>().getPlayerInstance().transform.position,Quaternion.identity);
        }
        return b;
    }

    public void Release(Bullet bullet)
    {
        if(bullet.GetBulletType() == BulletType.PlayerBullet)
        {
            playerBulletCache.Enqueue(bullet);
        }
        else if(bullet.GetBulletType() == BulletType.DiagonalBullet)
        {
            diagonalBulletCache.Enqueue(bullet);
        }
        else if(bullet.GetBulletType() == BulletType.SpiralBullet)
        {
            spiralBulletCache.Enqueue(bullet);
        }
        else
        {
            enemyBulletCache.Enqueue(bullet);
        }
        bullet.gameObject.SetActive(false);
    }

    public static BulletFactory Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        BulletFactory.Instance = this;
    }
}
