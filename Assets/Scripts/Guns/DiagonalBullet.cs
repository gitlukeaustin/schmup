﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiagonalBullet : Bullet
{
    Vector2 AxisDirection = new Vector2(1,1);

    [SerializeField]
    float MoveSpeed = 3.0f;

    int direction = 1;

    // Start is called before the first frame update
    void Start()
    {
                
    }

    // Update is called once per frame
    void Update()
    {
        transform.position  += new Vector3(MoveSpeed * AxisDirection.x * Time.deltaTime,direction * MoveSpeed * AxisDirection.y * Time.deltaTime,0);   
    }

    public void SwitchDirection()
    {
        direction *= -1;
    }
}
