﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralBullet : Bullet
{
    float fib1 = 1.0f;
    float fib2 = 1.0f;
    float counter = 2.0f;
    float newCounter = 2.0f;

    bool toggleAxis = true;

    Vector2 AxisDirection = new Vector2(1,1);

    [SerializeField]
    float MoveSpeed = 4.0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.position  += new Vector3(MoveSpeed * AxisDirection.x * Time.deltaTime,MoveSpeed * AxisDirection.y * Time.deltaTime,0);   
        UpdateFib();
    }

    void OnEnable()
    {
        fib1 = 1.0f;
        fib2 = 1.0f;
        counter = 2.0f;
        newCounter = 2.0f;

        toggleAxis = true;

        AxisDirection = new Vector2(1,1);

    }

    void UpdateFib()
    {
        counter = counter - 5.0f * Time.deltaTime;
        if(counter <= 0)
        {
            counter = fib1 + fib2;
            fib1 = fib2;
            fib2 = newCounter;
            newCounter = counter;
            if(toggleAxis)
            {
                AxisDirection.x *= -1;
            }
            else
            {
                AxisDirection.y *= -1;
            }
            toggleAxis = !toggleAxis;
        }
    }
}
