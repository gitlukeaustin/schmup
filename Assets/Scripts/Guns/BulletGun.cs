﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletGun : MonoBehaviour
{
    
    Vector2 Speed;
    
    float Cooldown = 0.0f;

    [SerializeField]
    float FiringRate = 0.5f;

    [SerializeField]
    GameObject gameManager;

    [SerializeField]
    float RechargeSpeed = 0.09f;

    bool locked = false;

  
    BulletFactory BulletFactory;

    BulletType activeBullet = BulletType.PlayerBullet;
    Queue<BulletType> bQueue;

    // Start is called before the first frame update
    void Start()
    {
        bQueue = new Queue<BulletType>();
        bQueue.Enqueue(BulletType.DiagonalBullet);
        bQueue.Enqueue(BulletType.SpiralBullet);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Fire()
    {
        if(GetComponent<PlayerAvatar>().GetEnergy() >= 1.0f)
        {
            locked = false;
        }
        
        if(Cooldown <= 0 && GetComponent<PlayerAvatar>().GetEnergy() > 0.0f && !locked)
        {
            GameObject b = BulletFactory.Instance.GetBullet(activeBullet);
            b.transform.position = transform.position;
          
            if(activeBullet == BulletType.DiagonalBullet)
            {
                FireDiagonalClone();
                GetComponent<PlayerAvatar>().DecreaseEnergy(b.GetComponent<DiagonalBullet>().GetEnergy());
            }
            else if(activeBullet == BulletType.SpiralBullet)
            {
                GetComponent<PlayerAvatar>().DecreaseEnergy(b.GetComponent<SpiralBullet>().GetEnergy());
            }
            else
            {
                GetComponent<PlayerAvatar>().DecreaseEnergy(b.GetComponent<SimpleBullet>().GetEnergy());
            }
            Cooldown = 5.0f;
            InvokeRepeating("DecreaseCooldown",0.01f,RechargeSpeed);
        }

        if(GetComponent<PlayerAvatar>().GetEnergy() <= 0.0f)
        {
            Debug.Log("energy is zero");
            locked = true;
        }


    }

    public void FireDiagonalClone()
    {
        GameObject b = BulletFactory.Instance.GetBullet(BulletType.DiagonalBullet);
        b.transform.position = transform.position;
     
        b.GetComponent<DiagonalBullet>().SwitchDirection();

    }

    public void SwitchBullet()
    {
        bQueue.Enqueue(activeBullet);
        activeBullet = bQueue.Dequeue();
    }

    void DecreaseCooldown()
    {
        Cooldown -= FiringRate;
        if(Cooldown <= 0)
        {
            CancelInvoke("DecreaseCooldown");
        }
    }
}
