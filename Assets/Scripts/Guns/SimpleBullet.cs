﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : Bullet
{
    [SerializeField]
    bool movesRight;

    [SerializeField]
    float MoveSpeed = 8.0f;

    // Start is called before the first frame update
    void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {
        int direction = -1;
        if(movesRight)
        {
            direction = 1;
        }
        transform.position  += new Vector3(direction * MoveSpeed * Time.deltaTime,0,0);   
    }
}
