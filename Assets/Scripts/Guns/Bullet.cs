﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletType {PlayerBullet, EnemyBullet, DiagonalBullet, SpiralBullet};



public abstract class Bullet : MonoBehaviour
{
    BulletFactory BulletFactory;

    [SerializeField]
    GameObject gameManager;


    
    [SerializeField]
    float Damage = 0.2f;

    [SerializeField]
    float Energy = 0.3f;

    public float GetEnergy()
    {
        return Energy;
    }
    
    protected Vector2 Position;

    [SerializeField]
    BulletType type;


    public BulletType GetBulletType()
    {
        return type;
    }    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public virtual void Init()
    {

    }

    public virtual void UpdatePosition()
    {

    }

    void OnBecameInvisible()
    {
        BulletFactory.Instance.Release(this);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player" && type == BulletType.EnemyBullet)
        {
            col.gameObject.GetComponent<PlayerAvatar>().TakeDamage(Damage);
            BulletFactory.Instance.Release(this);

        }
        else if(col.gameObject.tag == "Enemy" && type != BulletType.EnemyBullet)
        {
            Debug.Log("Enemy HIT!" + Damage);
            col.gameObject.GetComponent<EnemyAvatar>().TakeDamage(Damage);
            BulletFactory.Instance.Release(this);
        }
    }
}
